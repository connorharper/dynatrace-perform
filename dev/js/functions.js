// jquery plugin firings, amongst other things

//tab function for menu
$( document ).ready(function() {

   $( "#perform__subnav" ).tabs();
   $( "#agenda-tabs" ).tabs();

    //FAQ collapsible section
    $( ".faq-accordion" ).accordion({
        active: false,
        collapsible: true
    }),

    //mobile nav
    $('#perform__subnav--navigation').slicknav({
        closeOnClick: true
    }),

    $("#view-agenda").click(function() {
        if (screen.width > 1270) {        
            $(window).scrollTop(740);
            $("#perform__subnav").tabs("option", "active", 1);
        } else {
            $(this.hash).show();
        }      
    });

    $("#register-btn").click(function() {
        if (screen.width > 1270) {
            $(window).scrollTop(740);
            $("#perform__subnav").tabs("option", "active", 0);
        } else {
            $(this.hash).show();
        }      
    });

    //UX fix so sticky menu stays on top for each click
    $(".ui-tabs-anchor").click(function() {
        if (screen.width > 1270) {
            $(window).scrollTop(740);
        } else {
            $(this.hash).show();
        }    
    });

});

//fixed menu based on scroll position
$(window).scroll(function() {    
var scroll = $(window).scrollTop();

	if (scroll >= 740) {
    	$("#perform__subnav--navigation").addClass("fixedPos");
    	$(".perform__logo--navigation").addClass("perform__logo--navigation-visible");
        $(".container").css("padding-top", "6rem");
	}

	else {
    	$("#perform__subnav--navigation").removeClass("fixedPos");
    	$(".perform__logo--navigation").removeClass("perform__logo--navigation-visible");
        $(".container").css("padding-top", "0");
	}

});
