const gulp = require('gulp');
	uglify = require('gulp-uglify'),
	sass = require('gulp-ruby-sass');
    cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const child = require('child_process');
const gutil = require('gulp-util');
const siteRoot = '_site';

//default task
gulp.task('default', ['scripts', 'styles', 'jekyll', 'serve']);

//scripts task, also "Uglifies" JS
gulp.task('scripts', function() {
	gulp.src('dev/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('build/js'))
	// .pipe(browserSync.stream());
});

//styles build/minification
gulp.task('styles', function() {
	return sass('dev/scss/styles.scss')
	.pipe(cleanCSS())
	.pipe(gulp.dest('build/css'))
	// .pipe(browserSync.stream());
});

//server + file watching 
gulp.task('serve', function() {

    browserSync.init({
        server: "_site"
    });

    gulp.watch("dev/scss/*.scss", ['styles', 'jekyll']);
    gulp.watch('dev/js/*.js', ['scripts', 'jekyll']);
    gulp.watch('_includes/*.html', ['jekyll']).on('change', browserSync.reload);
    gulp.watch('_site').on('change', browserSync.reload);

});

// The way Node behaves on Windows is different from Mac. I wrote this gulpfile on a mac,
// you'll need to switch different 'jekyll' tasks depending on your OS. 

// // if you are mac, comment the other task and use this task
gulp.task('jekyll', function(gulpCallBack){
    var spawn = require('child_process').spawn;
    // After build: cleanup HTML
    var jekyll = spawn('jekyll', ['build'], {stdio: 'inherit'});

    jekyll.on('exit', function(code) {
        gulpCallBack(code === 0 ? null : 'ERROR: Jekyll process exited with code: '+code);
    });
});


// //If you're PC, Use this task and comment out the mac one
// gulp.task('jekyll', function(gulpCallBack){
//     var spawn = require('child_process').spawn;
//     // After build: cleanup HTML

//     //Windows fix for this child process
//     var jekyll = spawn('jekyll.bat', ['build'], {stdio: 'inherit'});

//     jekyll.on('exit', function(code) {
//         gulpCallBack(code === 0 ? null : 'ERROR: Jekyll process exited with code: '+code);
//     });
// });